package com.example.em.ejemplobluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Message;
import android.os.Handler;
import android.os.ParcelUuid;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;


public class ListActivity extends ActionBarActivity {
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;
    private Button button;
    private ArrayAdapter adapter;
    private ListView lv;
    private static final int ENABLE_BT_REQUEST_CODE = 1;
    private static final int DISCOVERABLE_BT_REQUEST_CODE = 2;
    private static final int DISCOVERABLE_DURATION = 300;
    BluetoothAdapter mBluetoothAdapter = setAdapter();
    String MAC;
    String tag = "debugging";
    protected static final int SUCCESS_CONNECT = 0;
    protected static final int MESSAGE_READ = 1;
    IntentFilter filter;
    private boolean SERVER_CONNECTED;
    private boolean CLIENT_CONNECTED;
    BluetoothDevice bluetoothDevice;
    BluetoothSocket bluetoothSocket;
    BluetoothSocket temp = null;
    private int mState = STATE_NONE;
    public static final int STATE_NONE = 0;       // we're doing nothing
    public static final int STATE_LISTEN = 1;     // now listening for incoming connections
    public static final int STATE_CONNECTING = 2; // now initiating an outgoing connection
    public static final int STATE_CONNECTED = 3;  // now connected to a remote device
    private ConnectingThread mConnectingThread;
    private ConnectedThread mConnectedThread;
    private static final UUID MY_UUID_INSECURE =
            UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");
    private static final UUID MY_UUID_SECURE =
            UUID.fromString("fa87c0d0-afac-11de-8a39-0800200c9a66");
    private static final String NAME_SECURE = "BluetoothChatSecure";
    private static final String NAME_INSECURE = "BluetoothChatInsecure";
    private ListeningThread mSecureListeningThread;
    private ListeningThread mInsecureListeningThread;
    public static String EXTRA_DEVICE_ADDRESS = "device_address";
    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {

                case MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String string = new String(readBuf);
                    Toast.makeText(getApplicationContext(), string, Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    final BroadcastReceiver mReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();
            // When discovery finds a device
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice bluetoothDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // Add the name and address to an array adapter to show in a ListView
                adapter.add(bluetoothDevice.getName() + "\n" + bluetoothDevice.getAddress());
            }
        }
    };

    public static BluetoothAdapter setAdapter() {
        BluetoothAdapter mbluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return mbluetoothAdapter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        lv = (ListView) findViewById(R.id.ListView1);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // ListView Clicked item value

                    String itemValue = (String) lv.getItemAtPosition(position);
                    MAC = itemValue.substring(itemValue.length() - 17);
                BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(MAC);
                    ConnectingThread ct = new ConnectingThread(device,true);
                    ct.start();
                    // Initiate a connection request in a separate thread
            }
        });
        adapter = new ArrayAdapter
                (this, android.R.layout.simple_list_item_1);
        lv.setAdapter(adapter);
        button = (Button) findViewById(R.id.button3);

        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
// If there are paired devices
        if (pairedDevices.size() > 0) {
            // Loop through paired devices
            for (BluetoothDevice device : pairedDevices) {
                // Add the name and address to an array adapter to show in a ListView
                adapter.add(device.getName() + "\n" + device.getAddress());
            }
        }

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        this.registerReceiver(mReceiver, filter);
    }
    public synchronized void start() {

        // Cancel any thread attempting to make a connection
        if (mConnectingThread != null) {
            mConnectingThread.cancel();
            mConnectingThread = null;
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        setState(STATE_LISTEN);

        // Start the thread to listen on a BluetoothServerSocket
        if (mSecureListeningThread == null) {
            mSecureListeningThread  = new ListeningThread(true);
            mSecureListeningThread .start();
        }
        if (mInsecureListeningThread == null) {
            mInsecureListeningThread  = new ListeningThread(false);
            mInsecureListeningThread.start();
        }
    }
    private synchronized void setState(int state) {
        mState = state;
    }
    public synchronized void connect(BluetoothDevice device, boolean secure) {

        // Cancel any thread attempting to make a connection
        if (mState == STATE_CONNECTING) {
            if (mConnectingThread != null) {
                mConnectingThread.cancel();
                mConnectingThread = null;
            }
        }

        // Cancel any thread currently running a connection
        if (mConnectedThread != null) {
            mConnectedThread.cancel();
            mConnectedThread = null;
        }

        // Start the thread to connect with the given device
        mConnectingThread = new ConnectingThread(device, secure);
        mConnectingThread.start();
        setState(STATE_CONNECTING);
    }
    public void onToggleClicked(View view) {
        ToggleButton toggleButton = (ToggleButton) view;
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            Toast.makeText(getApplicationContext(), "Oop! Your device does not support Bluetooth",
                    Toast.LENGTH_SHORT).show();
            toggleButton.setChecked(false);
        } else {

            if (toggleButton.isChecked()) { // to turn on bluetooth
                if (!mBluetoothAdapter.isEnabled()) {
                    // A dialog will appear requesting user permission to enable Bluetooth
                    Intent enableBluetoothIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBluetoothIntent, ENABLE_BT_REQUEST_CODE);
                } else {
                    Toast.makeText(getApplicationContext(), "Your device has already been enabled." +
                                    "\n" + "Scanning for remote Bluetooth devices...",
                            Toast.LENGTH_SHORT).show();
                    // To discover remote Bluetooth devices
                    ListeningThread lt = new ListeningThread(true);
                    lt.start();
                    discoverDevices();
                    // Make local device discoverable by other devices
                    makeDiscoverable();
                }
            }
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Make sure we're not doing discovery anymore
        if (mBluetoothAdapter != null) {
            mBluetoothAdapter.cancelDiscovery();
        }

        // Unregister broadcast listeners
        unregisterReceiver(mReceiver);

    }
    public synchronized int getState() {
        return mState;
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (mConnectedThread != null || mConnectingThread != null || mInsecureListeningThread != null ||mSecureListeningThread != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (getState() == STATE_NONE) {
                // Start the Bluetooth chat services
                start();
            }
        }
        // Register the BroadcastReceiver for ACTION_FOUND
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        this.registerReceiver(mReceiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.unregisterReceiver(mReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return false;
    }
    private void connectDevice(Intent data,boolean secure) {
        // Get the device MAC address
        // Get the BluetoothDevice object
        String address = data.getExtras()
                .getString(EXTRA_DEVICE_ADDRESS);
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        connect(device, secure);
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data,true);
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data,false);
                }
                break;
            case REQUEST_ENABLE_BT:

                // Bluetooth successfully enabled!
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(getApplicationContext(), " Bluetooth is now enabled." +
                                    "\n" + "Scanning for remote Bluetooth devices...",
                            Toast.LENGTH_SHORT).show();

                    // To discover remote Bluetooth devices
                    discoverDevices();

                    // Make local device discoverable by other devices
                    makeDiscoverable();

                    // Start a thread to create a  server socket to listen
                    // for connection request


                } else { // RESULT_CANCELED as user refused or failed to enable Bluetooth
                    Toast.makeText(getApplicationContext(), "Bluetooth is not enabled.",
                            Toast.LENGTH_SHORT).show();


                }
                if (requestCode == DISCOVERABLE_BT_REQUEST_CODE) {

                    if (resultCode == DISCOVERABLE_DURATION) {
                        Toast.makeText(getApplicationContext(), "Your device is now discoverable by other devices for " +
                                        DISCOVERABLE_DURATION + " seconds",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Fail to enable discoverability on your device.",
                                Toast.LENGTH_SHORT).show();
                    }
                }
        }
    }

    protected void discoverDevices() {
        // To scan for remote Bluetooth devices
        if (mBluetoothAdapter.startDiscovery()) {
            Toast.makeText(getApplicationContext(), "Discovering other bluetooth devices...",
                    Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getApplicationContext(), "Discovery failed to start.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    protected void makeDiscoverable() {
        // Make local device discoverable
        Intent discoverableIntent = new
                Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, DISCOVERABLE_DURATION);
        startActivityForResult(discoverableIntent, DISCOVERABLE_BT_REQUEST_CODE);
    }

    // Create a BroadcastReceiver for ACTION_FOUND
    public void send(View v) {
        String s;
        try {

                EditText mEdit;
                mEdit = (EditText) findViewById(R.id.editText2);
                s = mEdit.getText().toString();
                ConnectedThread ct = new ConnectedThread(bluetoothSocket);


            if (SERVER_CONNECTED) {

                ct.write(s.getBytes());
            }

            if (CLIENT_CONNECTED) {
                ct.write(s.getBytes());
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Activate your bluetooth adapter first or make sure you're connected to another device.",
                    Toast.LENGTH_SHORT).show();
        }
    }




    private class ListeningThread extends Thread {
        final int MESSAGE_READ = 9999;

        private final BluetoothServerSocket bluetoothServerSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;

        public ListeningThread(boolean secure) {
            BluetoothServerSocket temp = null;
            try {
                if (secure) {
                    temp = mBluetoothAdapter.listenUsingRfcommWithServiceRecord(NAME_SECURE,
                            MY_UUID_SECURE);
                } else {
                    temp =mBluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(
                            NAME_INSECURE, MY_UUID_INSECURE);
                }
            } catch (IOException e) {

            }
            bluetoothServerSocket = temp;
            try {
                tmpIn = bluetoothSocket.getInputStream();
                tmpOut = bluetoothSocket.getOutputStream();
            } catch (IOException e) {
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;


        }

        public void run() {
            byte[] buffer = new byte[1024];  // buffer store for the stream
            int bytes; // bytes returned from read()
            // This will block while listening until a BluetoothSocket is returned
            // or an exception occurs
            while (true) {
                try {
                    bluetoothSocket = bluetoothServerSocket.accept();
                    SERVER_CONNECTED = true;
                } catch (IOException e) {
                    Toast.makeText(getApplicationContext(), "Error1",
                            Toast.LENGTH_SHORT).show();
                    break;
                }

                // If a connection is accepted
                if (bluetoothSocket != null) {

                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(getApplicationContext(), "A connection has been accepted.",
                                    Toast.LENGTH_SHORT).show();

                            try {
                                bluetoothServerSocket.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }


                        }


                    });

                    try {
                        // Read from the InputStream
                        bytes = mmInStream.read(buffer);
                        TextView textview = (TextView) findViewById(R.id.textView);
                        StringBuilder sb = new StringBuilder();
                        BufferedReader br = null;
                        String line;
                        while (true) {
                            try {
                                br = new BufferedReader(new InputStreamReader(mmInStream));
                                line = br.readLine();

                                mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer)
                                        .sendToTarget();


                            } catch (IOException e) {
                                Toast.makeText(getApplicationContext(), "Error2",
                                        Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                                break;
                            }
                        }
                        // Send the obtained bytes to the UI activity

                    } catch (IOException e) {
                        Toast.makeText(getApplicationContext(), "Error3",
                                Toast.LENGTH_SHORT).show();
                        break;
                    }

                    try {
                        bluetoothServerSocket.close();
                    } catch (IOException e) {
                        Toast.makeText(getApplicationContext(), "Error4",
                                Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }


        // Cancel the listening socket and terminate the thread
        public void cancel() {
            try {
                bluetoothServerSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class ConnectingThread extends Thread {
        private BluetoothSocket bluetoothSocket;
        private final BluetoothDevice bluetoothDevice;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;


        public ConnectingThread(BluetoothDevice device, boolean secure) {


            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            bluetoothDevice = device;

            // Get a BluetoothSocket to connect with the given BluetoothDevice
            try {
                if (secure) {
                    temp = device.createRfcommSocketToServiceRecord(
                            MY_UUID_SECURE);
                } else {
                    temp = device.createInsecureRfcommSocketToServiceRecord(
                            MY_UUID_INSECURE);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            bluetoothSocket = temp;
            try {
                tmpIn = bluetoothSocket.getInputStream();
                tmpOut = bluetoothSocket.getOutputStream();
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Error6",
                        Toast.LENGTH_SHORT).show();
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }


        public void run() {
            // Cancel discovery as it will slow down the connection
            byte[] buffer = new byte[1024];  // buffer store for the stream
            int bytes; // bytes returned from read()
            mBluetoothAdapter.cancelDiscovery();

            try {

                // This will block until it succeeds in connecting to the device
                // through the bluetoothSocket or throws an exception
                bluetoothSocket = (BluetoothSocket) bluetoothDevice.getClass().getMethod("createRfcommSocket", new Class[] {int.class}).invoke(bluetoothDevice,1);
                bluetoothSocket.connect();
                CLIENT_CONNECTED = true;

                // Read from the InputStream


                while (true) {
                    try {
                        buffer = new byte[1024];
                        bytes = mmInStream.read(buffer);
                        // Send the obtained bytes to the UI activity
                        mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer)
                                .sendToTarget();


                    } catch (IOException e) {
                        Toast.makeText(getApplicationContext(), "Error8",
                                Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                        break;
                    }
                }

            } catch (IOException connectException) {
                connectException.printStackTrace();

            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }


        // Cancel an open connection and terminate the thread
        public void cancel() {
            try {
                bluetoothSocket.close();
            } catch (IOException e) {
                Toast.makeText(getApplicationContext(), "Error10",
                        Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            byte[] buffer = new byte[1024];  // buffer store for the stream
            int bytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    // Read from the InputStream
                    bytes = mmInStream.read(buffer);
                    // Send the obtained bytes to the UI activity
                    mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer)
                            .sendToTarget();
                } catch (IOException e) {
                    break;
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        public void write(byte[] bytes) {
            try {
                mmOutStream.write(bytes);
            } catch (IOException e) {
            }
        }

        /* Call this from the main activity to shutdown the connection */
        public void cancel() {
            try {
                mmSocket.close();
                mmInStream.close();
                mmOutStream.close();
            } catch (IOException e) {
            }
        }
    }
}

