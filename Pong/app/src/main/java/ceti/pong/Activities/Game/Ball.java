package ceti.pong.Activities.Game;

import android.content.Context;
import android.graphics.Rect;
import android.widget.ImageView;

public class Ball {
    private final ImageView imgvBall;
    private final Rect limits;
    private final Bar bar;
    private double x, y;
    private double angle;
    private final int displayHeight, displayWidth;
    private final Context context;

    public Ball(Context context, ImageView imgvBall, Rect limits, Bar bar, double angle, int displayHeight, int displayWidth) {
        this.context = context;
        this.imgvBall = imgvBall;
        this.limits = limits;
        this.bar = bar;
        x = imgvBall.getX();
        y = imgvBall.getY();
        this.angle = angle;
        this.displayHeight = displayHeight;
        this.displayWidth = displayWidth;
    }

    public void move() {
        if (x  - 25 <= limits.left) {
            angle = Math.PI - angle;
            //TODO SendMessage
        }
        if (x + 10 >= bar.getX()) {
            if (y >= bar.getY() && y <= bar.getY() + bar.getHeight()) {
                angle = Math.PI - angle;
            } else {
                angle = Math.PI - angle;
                ((GameActivity) context).loseBall();
            }
        }
        if (y - displayHeight * .02 <= limits.top) {
            angle = 2 * Math.PI - angle;
        }
        if (y >= limits.bottom * .94 - 52) {
            angle = 2 * Math.PI - angle;
        }
        x += Math.cos(angle) * 2;
        y += -Math.sin(angle) * 2;
        imgvBall.setX((float) x);
        imgvBall.setY((float) y);
    }
}
