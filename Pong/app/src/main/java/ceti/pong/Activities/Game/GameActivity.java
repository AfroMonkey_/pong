package ceti.pong.Activities.Game;

import android.content.SharedPreferences;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Random;
import java.util.prefs.Preferences;

import ceti.pong.R;
import ceti.pong.ScoreDataBase.Score;
import ceti.pong.ScoreDataBase.ScoreDataSource;

public class GameActivity extends AppCompatActivity implements SensorEventListener {
    private Bar bar;
    private Ball ball;
    private boolean isMoving = true; //TODO change with message
    private int dir = 0;
    private TextView lblVSScore;
    private TextView lblPlayerScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);


        ImageView imgvBar = (ImageView) findViewById(R.id.imgvBar);
        ImageView imgvBall = (ImageView) findViewById(R.id.imgvBall);
        lblVSScore = (TextView) findViewById(R.id.lblVSScore);
        lblPlayerScore = (TextView) findViewById(R.id.lblPlayerScore);

        imgvBar.setX(getResources().getDisplayMetrics().widthPixels * .9f - 28);
        imgvBar.setY(getResources().getDisplayMetrics().heightPixels / 2 - 125 / 2);

        imgvBall.setX(getResources().getDisplayMetrics().widthPixels * .8f - 28);
        imgvBall.setY(getResources().getDisplayMetrics().heightPixels / 2 - 125 / 2);

        bar = new Bar(imgvBar, getResources().getDisplayMetrics().heightPixels);
        Rect limits = new Rect();
        limits.top = 0;
        limits.right = getResources().getDisplayMetrics().widthPixels;
        limits.bottom = getResources().getDisplayMetrics().heightPixels;
        limits.left = 0;
        float top1 = 1.75f;
        float top2 = 1.25f;

        Random in = new Random();
        float angIn = in.nextFloat() * (top2 - top1) + top2;
        angIn = (float) (Math.PI * (angIn + 1));


        ball = new Ball(this, (ImageView) findViewById(R.id.imgvBall), limits, bar, angIn, getResources().getDisplayMetrics().heightPixels, getResources().getDisplayMetrics().widthPixels);

        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        while (isMoving) {
                            wait(5);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ball.move();
                                    bar.move(dir);
                                }
                            });
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    public void loseBall() {
        int vsScore = Integer.valueOf(lblVSScore.getText().toString());
        vsScore++;
        lblVSScore.setText(String.valueOf(vsScore));
        if(vsScore == 5) {
            lose();
        }
    }

    public void lose() {
        ScoreDataSource scoreDataSource = new ScoreDataSource(this);
        scoreDataSource.open();
        Score score = new Score();
        SharedPreferences preferences = (SharedPreferences) getSharedPreferences("PreferencesPong", MODE_PRIVATE);
        score.setPlayer(preferences.getString("Player", "TU"));
        score.setScore(0);
        score.setVs("Enemigo");
        score.setVsScore(Integer.valueOf(lblVSScore.getText().toString()));
        scoreDataSource.insertScore(score);
        scoreDataSource.close();
        this.finish();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        synchronized (this) {
            float curY = event.values[1];
            if (curY < -1.5) {
                dir = Bar.TOP;
            }
            if (curY > 1.5) {
                dir = Bar.BOTTOM;
            }
            if (curY > -1.5 && curY < 1.5) {
                dir = Bar.NONE;
            }
        }
    }

    @Override
    public void onAccuracyChanged(android.hardware.Sensor sensor, int accuracy) {
    }

    @Override
    protected void onResume() {
        super.onResume();
        SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        List<Sensor> sensors = sm.getSensorList(Sensor.TYPE_ACCELEROMETER);
        if (sensors.size() > 0) {
            sm.registerListener(this, sensors.get(0), SensorManager.SENSOR_DELAY_GAME);
        }
    }

    @Override
    protected void onStop() {
        SensorManager sm = (SensorManager) getSystemService(SENSOR_SERVICE);
        sm.unregisterListener(this);
        super.onStop();
    }
}
