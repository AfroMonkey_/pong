package ceti.pong.Activities.Score;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ceti.pong.R;
import ceti.pong.ScoreDataBase.Score;

public class ScoreRecyclerAdapter extends RecyclerView.Adapter<ScoreRecyclerAdapter.ViewHolder> {

    private List<Score> scores;

    public ScoreRecyclerAdapter(List<Score> scores) {
        this.scores = scores;
    }

    @Override
    public ScoreRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.score_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ScoreRecyclerAdapter.ViewHolder holder, int position) {
        Score score = scores.get(position);
        holder.vPlayerOne.setText(score.getPlayer());
        holder.vPlayerTwo.setText(score.getVs());
        holder.vScoreOne.setText(String.valueOf(score.getScore()));
        holder.vScoreTwo.setText(String.valueOf(score.getVsScore()));
    }

    @Override
    public int getItemCount() {
        return scores.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView vPlayerOne;
        public TextView vPlayerTwo;
        public TextView vScoreOne;
        public TextView vScoreTwo;

        public ViewHolder(View itemView) {
            super(itemView);
            vPlayerOne = (TextView) itemView.findViewById(R.id.vYouScore);
            vPlayerTwo = (TextView) itemView.findViewById(R.id.vPlayerTwoNameScore);
            vScoreOne = (TextView) itemView.findViewById(R.id.vPlayerOneScore);
            vScoreTwo = (TextView) itemView.findViewById(R.id.vPlayerTwoScore);
        }
    }
}
