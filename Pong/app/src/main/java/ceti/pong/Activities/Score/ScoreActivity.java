package ceti.pong.Activities.Score;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import ceti.pong.R;
import ceti.pong.ScoreDataBase.Score;
import ceti.pong.ScoreDataBase.ScoreDataSource;

public class ScoreActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_score);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerViewScore);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        ScoreDataSource dataSource = new ScoreDataSource(this);
        dataSource.open();
        List<Score> scores = dataSource.getScores();
        dataSource.close();
        RecyclerView.Adapter adapter = new ScoreRecyclerAdapter(scores);
        recyclerView.setAdapter(adapter);

    }
}
