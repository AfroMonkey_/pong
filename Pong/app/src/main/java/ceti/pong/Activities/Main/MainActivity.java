package ceti.pong.Activities.Main;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ceti.pong.Activities.Game.GameActivity;
import ceti.pong.Activities.Score.ScoreActivity;
import ceti.pong.R;
import ceti.pong.ScoreDataBase.Score;
import ceti.pong.ScoreDataBase.ScoreDataSource;

public class MainActivity extends AppCompatActivity {
    private TextView lblPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lblPlayer = (TextView) findViewById(R.id.lblPlayer);
        lblPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPlayer();
            }
        });
        ImageView btnPlay = (ImageView) findViewById(R.id.btnPlay);
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent scoreActivity = new Intent(MainActivity.this, GameActivity.class);
                startActivity(scoreActivity);
            }
        });
        ImageView btnScore = (ImageView) findViewById(R.id.btnScore);
        btnScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent scoreActivity = new Intent(MainActivity.this, ScoreActivity.class);
                startActivity(scoreActivity);
            }
        });

        final SharedPreferences preferences = getSharedPreferences("PreferencesPong", MODE_PRIVATE);
        String player = preferences.getString("Player", "");
        if (player != null && player.equals("")) {
            setPlayer();
        } else {
            lblPlayer.setText(player);
        }
    }

    public String getPlayer() {
        final SharedPreferences preferences = getSharedPreferences("PreferencesPong", MODE_PRIVATE);
        return preferences.getString("Player", "");
    }

    private void setPlayer() {
        final SharedPreferences preferences = getSharedPreferences("PreferencesPong", MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Jugador");
        final EditText input = new EditText(MainActivity.this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (input.getText().toString().equals("")) {
                    setPlayer();
                }
                editor.putString("Player", input.getText().toString());
                editor.apply();
                lblPlayer.setText(input.getText().toString());
            }
        });
        builder.show();
    }
}
