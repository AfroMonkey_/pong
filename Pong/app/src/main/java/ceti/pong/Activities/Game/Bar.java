package ceti.pong.Activities.Game;

import android.util.Log;
import android.widget.ImageView;

public class Bar {
    private final ImageView imgvBar;
    private float x;
    private float height;
    private final float displayHeight;
    public static final int TOP = -2, NONE = 0, BOTTOM = 2;

    public Bar(ImageView imgvBar, int displayHeight) {
        this.imgvBar = imgvBar;
        this.displayHeight = displayHeight;
        x = imgvBar.getX();
        height = 125;
    }

    public void move(int dir) {
        switch (dir) {
            case TOP: {
                if (imgvBar.getY() > displayHeight * .02) {
                    imgvBar.setY(imgvBar.getY() + dir);
                }
                break;
            }
            case NONE: {
                break;
            }
            case BOTTOM: {
                if (imgvBar.getY() < displayHeight * .94 - height) {
                    imgvBar.setY(imgvBar.getY() + dir);
                }
                break;
            }
            default: {
                Log.d("Bar.move()", "Movimiento invalido " + dir);
                break;
            }
        }
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return imgvBar.getY();
    }

    public float getHeight() {
        return height;
    }
}
