package ceti.pong.ScoreDataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import ceti.pong.ScoreDataBase.ScoreOpenHelper.TableScore;

public class ScoreDataSource {
    private SQLiteDatabase db;
    private ScoreOpenHelper scoreOpenHelper;

    public ScoreDataSource(Context context) {
        scoreOpenHelper = new ScoreOpenHelper(context);
    }

    public void open() {
        db = scoreOpenHelper.getWritableDatabase();
    }

    public void close() {
        scoreOpenHelper.close();
    }

    public List<Score> getScores() {
        List<Score> scores = new ArrayList<>();
        Cursor cursor = db.query(TableScore.NAME, new String[]{TableScore.COLUMN_PLAYER, TableScore.COLUMN_PLAYER_SCORE, TableScore.COLUMN_VS, TableScore.COLUMN_VS_SCORE}, null, null, null, null, TableScore.COLUMN_PLAYER);
        cursor.moveToFirst();
        Score score;
        while (!cursor.isAfterLast()) {
            score = new Score();
            score.setPlayer(cursor.getString(0));
            score.setScore(cursor.getInt(1));
            score.setVs(cursor.getString(2));
            score.setVsScore(cursor.getInt(3));
            scores.add(score);
            cursor.moveToNext();
        }
        cursor.close();
        return scores;
    }

    public void insertScore(Score score) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TableScore.COLUMN_PLAYER, score.getPlayer());
        contentValues.put(TableScore.COLUMN_PLAYER_SCORE, score.getScore());
        contentValues.put(TableScore.COLUMN_VS, score.getVs());
        contentValues.put(TableScore.COLUMN_VS_SCORE, score.getVsScore());
        db.insert(TableScore.NAME, null, contentValues);
    }
}
