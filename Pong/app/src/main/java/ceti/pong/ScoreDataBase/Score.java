package ceti.pong.ScoreDataBase;

public class Score {
    private int score;
    private String vs;
    private int vsScore;
    private String player;

    public Score() {
    }

    public Score(int score, String vs, int vsScore, String player) {
        this.score = score;
        this.vs = vs;
        this.vsScore = vsScore;
        this.player = player;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getVs() {
        return vs;
    }

    public void setVs(String vs) {
        this.vs = vs;
    }

    public int getVsScore() {
        return vsScore;
    }

    public void setVsScore(int vsScore) {
        this.vsScore = vsScore;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }
}
