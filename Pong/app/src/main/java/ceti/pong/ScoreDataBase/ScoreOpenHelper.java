package ceti.pong.ScoreDataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class ScoreOpenHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "Score";
    private static final String CREATE_TABLE_SCORE =
            "CREATE TABLE " + TableScore.NAME + "(" +
                    TableScore.COLUMN_PK + " " + TableScore.COLUMN_PK_TYPE + ", " +
                    TableScore.COLUMN_PLAYER + " " + TableScore.COLUMN_PLAYER_TYPE + ", " +
                    TableScore.COLUMN_PLAYER_SCORE + " " + TableScore.COLUMN_PLAYER_SCORE_TYPE + ", " +
                    TableScore.COLUMN_VS + " " + TableScore.COLUMN_VS_TYPE + ", " +
                    TableScore.COLUMN_VS_SCORE + " " + TableScore.COLUMN_VS_SCORE_TYPE +
                    ")";


    public ScoreOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_TABLE_SCORE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TableScore.NAME);
        onCreate(db);
    }

    //----------//
    public static class TableScore {
        public static final String NAME = "Score";
        public static final String COLUMN_PK = "PK";
        public static final String COLUMN_PK_TYPE = "INTEGER PRIMARY KEY AUTOINCREMENT";
        public static final String COLUMN_PLAYER = "Player";
        public static final String COLUMN_PLAYER_TYPE = "TEXT";
        public static final String COLUMN_PLAYER_SCORE = "Score";
        public static final String COLUMN_PLAYER_SCORE_TYPE = "INT";
        public static final String COLUMN_VS = "VS";
        public static final String COLUMN_VS_TYPE = "TEXT";
        public static final String COLUMN_VS_SCORE = "VsScore";
        public static final String COLUMN_VS_SCORE_TYPE = "INT";
    }
}
